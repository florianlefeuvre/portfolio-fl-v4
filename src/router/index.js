import Vue from 'vue';
import VueRouter from 'vue-router';
import HomePage from '../views/HomePage.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home-page',
    component: HomePage,
    meta: { pageClass: 'home-page' }
  },
  {
    path: '/mode-et-beaute',
    name: 'mode.list',
    component: () => import('../views/FashionPage.vue'),
    meta: { pageClass: 'mode-et-beaute' },
    children: [
      {
        path: ':id',
        name: 'mode.page',
        component: () => import('../views/ProjectPageSelector.vue')
      }
    ]
  },
  {
    path: '/street-et-reportages',
    name: 'street.list',
    component: () => import('../views/StreetPage.vue'),
    meta: { pageClass: 'street-et-reportages' },
    children: [
      {
        path: ':id',
        name: 'street.page',
        component: () => import('../views/ProjectPageSelector.vue')
      }
    ]
  },
  {
    path: '/a-propos',
    name: 'a-propos',
    // route level code-splitting
    // this generates a separate chunk (a-propos.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/AboutPage.vue'),
    meta: { pageClass: 'a-propos' }
  },
  {
    path: '/mentions-legales',
    name: 'mentions-legales',
    component: () => import('../views/TermsAndConditions.vue'),
    meta: { pageClass: 'mentions-legales' }
  },
  {
    path: '/404',
    name: '404',
    component: () => import('../views/NotFound.vue'),
    meta: { pageClass: 'page-not-found' }
  },
  { path: '*', redirect: '/404' }
];

const router = new VueRouter({
  mode: 'history',
  base: import.meta.env.BASE_URL.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    // Default scroll position will be 0 unless overridden by a saved position
    const position = {
      x: 0,
      y: 0
    };

    // Override default with saved position (if it exists)
    if (savedPosition) {
      position.x = savedPosition.x;
      position.y = savedPosition.y;
    }

    // Set scroll position after route transition (before enter)
    return new Promise(resolve => {
      this.app.$root.$once('scrollBeforeEnter', () => {
        resolve(position);
      });
    });
  },
  routes
});

export default router;
