export default {
  inserted: el => {
    function loadMedia() {
      const mediaElement = Array.from(el.children).find(
        el => el.nodeName === 'IMG' || el.nodeName === 'VIDEO'
      );
      if (mediaElement) {
        ['load', 'canplay'].forEach(e =>
          mediaElement.addEventListener(e, () => {
            el.classList.remove('loading');
          })
        );
        mediaElement.addEventListener('error', () => console.log('error'));
        mediaElement.src = mediaElement.dataset.src;
        mediaElement.removeAttribute('data-src');
      }
    }

    function handleIntersect(entries, observer) {
      entries.forEach(entry => {
        if (!entry.isIntersecting) {
          return;
        } else {
          loadMedia();
          observer.unobserve(el);
        }
      });
    }

    function createObserver() {
      const options = {
        root: null,
        treshold: 0
      };
      const observer = new IntersectionObserver(handleIntersect, options);
      observer.observe(el);
    }

    if (window['IntersectionObserver']) createObserver();
    else loadMedia();
  }
};
