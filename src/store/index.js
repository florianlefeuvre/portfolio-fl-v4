import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    //Debug and strict = true when environment is not in production
    debug: import.meta.env.NODE_ENV !== 'production',
    strict: import.meta.env.NODE_ENV !== 'production',
    windowSize: {
      width: 0,
      height: 0
    }
  },
  mutations: {
    SET_WINDOW_SIZE(state, size) {
      if (state.debug) console.log('SET_WINDOW_SIZE déclenchée :', size);
      state.windowSize.width = size.width;
      state.windowSize.height = size.height;
    }
  },
  getters: {
    getWindowSize: state => {
      return state.windowSize;
    }
  },
  actions: {}
});
