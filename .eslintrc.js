module.exports = {
  root: true,

  env: {
    node: true,
    es2022: true
  },

  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/prettier',
    'plugin:prettier/recommended'
  ],

  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/no-v-html': 'off'
  }
};
