/**
 *
 * "get-image-ratio" is a custom Node JS Script that checks if json data files
 * contains thumbnail img elements that should be displayed in a masonry list layout.
 * If any, the script probes the imgs width & height, calculates its ratios
 * and inject them into a new json data file. Thoses ratios can
 * then be used to display aspect-ratio friendly divs with CSS,
 * before loading imgs themselves in a lazy-loading masonry layout context.
 *
 */

'use strict';

const fs = require('fs');
const probe = require('probe-image-size');
const chalk = require('chalk');

const dataDirectoryPath = './src/data/';

console.log(chalk.bold.magenta('"Get-image-ratio" script launched...'));

console.log(
  chalk.yellow(
    '\n⏳  Searching for JSON data files from : ' + dataDirectoryPath
  )
);

let jsonFiles = [];
getJSONFiles(dataDirectoryPath);
console.log('\nJSON files in directory:', jsonFiles);

jsonFiles.forEach(jsonFile => {
  let masonryParams = checkForMasonryList(jsonFile);
  if (masonryParams)
    setImgRatio(masonryParams[0], masonryParams[1], masonryParams[2]);
});

console.log('\n✔️  Done');

/**
 * Crawl into a data directory
 * in search for JSON files
 * @param {String} dataDirectoryPath - path to the data directory
 */
function getJSONFiles(dataDirectoryPath) {
  fs.readdirSync(dataDirectoryPath).forEach(file => {
    let stat = fs.statSync(dataDirectoryPath + file);
    if (stat.isDirectory()) {
      return getJSONFiles(dataDirectoryPath + file + '/');
    } else if (
      file.split('_').pop() != 'MasonryLayout.json' &&
      file.split('.').pop() === 'json'
    ) {
      return jsonFiles.push(dataDirectoryPath + file);
    }
  });
}

/**
 * Parse a JSON file
 * @param {String} jsonFile - path to the jsonFile
 */
function readJSONFile(jsonFile) {
  let parsedFile = JSON.parse(fs.readFileSync(jsonFile));
  return parsedFile;
}

/**
 * Crawl into a JSON file
 * searching for masonryElements & assetsPath objects
 * @param {String} jsonFile - path to the jsonFile
 */
function checkForMasonryList(jsonFile) {
  console.log('\nSearching for masonry layouts in : ' + jsonFile);
  let file = readJSONFile(jsonFile);
  let masonryElements = file.masonryElements;
  if (masonryElements) {
    let assetsPath = './src/' + file.assetsPath + '/';
    if (assetsPath) {
      console.log(
        chalk.green(
          '-- This file does contain a masonry layout list and the followed assets path :',
          assetsPath
        )
      );
      return [masonryElements, assetsPath, jsonFile];
    } else {
      console.log(
        `${chalk.grey(
          '-- This file does contain a masonry layout list, '
        )} ${chalk.redBright(
          "but doesn't contain any assetsPath, please add one before reloading the script."
        )}`
      );
      return null;
    }
  } else {
    console.log(
      chalk.grey("-- This file doesn't contain any masonry layout list")
    );
    return null;
  }
}

/**
 * Crawl into a list of objects from a parsed JSON file
 * In each object, search for picture URL, then probe for picture width & height
 * @param {Array} masonryElements - a list of objects that should be displayed in a masonry list
 * @param {String} assetsPath - path to the assets linked to the JSON file
 * @param {String} jsonFile - path to the jsonFile
 */
function setImgRatio(masonryElements, assetsPath, jsonFile) {
  console.log(chalk.yellow('\n⏳  Probing elements of : ' + jsonFile));
  let jsonObj = [];
  masonryElements.forEach(el => {
    let imgId = el.id;
    if (imgId) {
      let imgDirectory = el.url;
      if (imgDirectory) {
        let imgFileName = el.thumbnail;
        if (imgFileName) {
          let imgPath = assetsPath + imgDirectory + '/' + imgFileName;
          let probedImg = probe.sync(fs.readFileSync(imgPath));
          let imgWidth = probedImg.width;
          let imgHeight = probedImg.height;
          let heightRatioPercentage = (imgHeight / imgWidth) * 100;
          console.log(
            chalk.grey(
              '\nId : ' +
                imgId +
                '\nAsset path : ' +
                imgPath +
                '\nWidth : ' +
                imgWidth +
                ', Height : ' +
                imgHeight +
                ', Width & height ratio percentage : ' +
                heightRatioPercentage
            )
          );
          jsonObj.push({
            id: imgId,
            boxRatio: heightRatioPercentage
          });
        } else
          console.log(
            chalk.redBright(
              '-- The object' +
                el.name +
                "doesn't contain any 'thumbnail' key, please add one before reloading the script."
            )
          );
      } else
        console.log(
          chalk.redBright(
            '-- The object' +
              el.name +
              "doesn't contain any 'url' key, please add one before reloading the script."
          )
        );
    } else
      console.log(
        chalk.redBright(
          '-- The object' +
            el.name +
            "doesn't contain any 'id' key, please add one before reloading the script."
        )
      );
  });
  if (jsonObj.length > 0) {
    fs.writeFileSync(
      jsonFile.split('.json').shift() + '_MasonryLayout.json',
      JSON.stringify(jsonObj)
    );
  } else
    console.log(
      chalk.redBright(
        'A list of masonry elements has been detected, but no elements have been found, please refer to the doc and correct your JSON file'
      )
    );
  console.log(
    chalk.green(
      '\nThe file ' +
        jsonFile.split('.json').shift() +
        '_MasonryLayout.json' +
        ' has been created with success.'
    )
  );
}
