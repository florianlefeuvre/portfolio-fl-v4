# Portfolio FL V4

> Site web réalisé en autodidaxie avec **Vue JS**, avant ma formation de développeur web, grâce à la merveilleuse documentation écrite par les membres de l'équipe **Vue**.

🔗 [**Cliquez ici pour voir le site**](https://florianlefeuvre.github.io)

Repository du portfolio destiné à mon activité de monteur vidéo 🎞 / retoucheur photo. 📸

- **Site temporairement hébergé sur une GitHub Page.**
  - *Le mode "History" du routeur front n'étant pas pris en charge par GitHub Page. Le site sera amené a être hébergé sur une autre plateforme à l'avenir.*

🔗 [*Cliquez ici pour voir mon profil GitHub*](https://github.com/florianlefeuvre)

Développé et designé avec ❤️ par mes soins.

[![Capture d'écran de la page d'accueil du portfolio](./docs/img/snapshot1.jpg)](https://florianlefeuvre.github.io)

[![Capture d'écran de la page Mode & Beauté du portfolio](./docs/img/snapshot2.jpg)](https://florianlefeuvre.github.io)

[![Capture d'écran de la page Street & Reportages du portfolio](./docs/img/snapshot3.jpg)](https://florianlefeuvre.github.io)